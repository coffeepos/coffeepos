﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;
using CoffePOST_App.ViewModels;
namespace CoffePOST_App.Views
{
    /// <summary>
    /// Interaction logic for LaporanScreen.xaml
    /// </summary>
    public partial class LaporanScreen : Window
    {
        Koneksi conn = new Koneksi();
        private SqlCommand cmd;
        private DataSet ds;
        private SqlDataAdapter da;
        private SqlDataReader dr;
        public LaporanScreen()
        {
            InitializeComponent();
        }
        void TampilData()
        {
            SqlConnection konn = conn.GetConn();
            konn.Open();
            cmd = new SqlCommand("SELECT id_pesanan AS 'NO PESANAN',status AS 'STATUS', item_terjual AS 'TERJUAL', total_harga AS 'TOTAL HARGA', id_user AS 'ID KASIR' FROM pesanan;", konn);
            //cmd = new SqlCommand("SELECT laporan.id_laporan AS [ID LAPORAN], pesanan.id_pesanan AS [ID PESANAN], pesanan.id_user AS [ID PEGAWAI], pesanan.total_harga AS [HARGA TOTAL] FROM laporan,pesanan;", konn);           
            da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("laporan barang");
            ds = new DataSet();

            da.Fill(dt);
            dataGrid.ItemsSource = dt.DefaultView;
            da.Update(dt);
        }

        void jmlhLaporan()
        {
            SqlConnection konn = conn.GetConn();
            cmd = new SqlCommand("SELECT COUNT(id_laporan) from laporan", konn);
            konn.Open();
            cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                textBlock.Text = dr[0].ToString();
            }
        }

        void pendapatan()
        {
            SqlConnection konn = conn.GetConn();
            cmd = new SqlCommand("SELECT SUM(total_harga) FROM pesanan", konn);
            konn.Open();
            cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                textBlock1.Text = dr[0].ToString();
            }
        }

        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            TampilData();
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            jmlhLaporan();
        }

        private void TextBlock1_Loaded(object sender, RoutedEventArgs e)
        {
            pendapatan();
        }
    }
}
