﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using CoffePOST_App.ViewModels;
using CoffePOST_App.Views;

using System.Data;
using System.Data.SqlClient;

namespace CoffePOST_App.Views
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        Koneksi konn = new Koneksi(); // membuat object koneksi
        private SqlCommand cmd; // perintah untuk query ke database
        private DataSet ds;
        private SqlDataAdapter da;
        private SqlDataReader dr;

        public LoginScreen()
        {
            InitializeComponent();
        }

        private void txtUsername_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text == "username@company.id")
            {
                txtUsername.Text = string.Empty;
            }
        }

        private void txtUsername_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text == string.Empty)
            {
                txtUsername.Text = "username@company.id";
            }
        }

        private void txtPass_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtPass.Text == string.Empty)
            {
                txtPass.Text = "masukkan password";
            }
        }

        private void txtPass_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtPass.Text == "masukkan password")
            {
                txtPass.Text = string.Empty;
            }

        }

        private void btnMasuk_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection conn = konn.GetConn();
            conn.Open();
            cmd = new SqlCommand("SELECT username,password from pengguna WHERE username='" + txtUsername.Text + "' and password='" + txtPass.Text + "'", conn);
            da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                DashboardScreen dashboard = new DashboardScreen();
                dashboard.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Login Gagal");
            }
            conn.Close();
        }
    }
}
