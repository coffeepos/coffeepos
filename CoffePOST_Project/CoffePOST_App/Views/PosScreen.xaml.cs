﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data.SqlClient;
using System.Data;
using CoffePOST_App.ViewModels;

namespace CoffePOST_App.Views
{
    /// <summary>
    /// Interaction logic for PosScreen.xaml
    /// </summary>
    public partial class PosScreen : Window
    {
        Koneksi konn = new Koneksi(); // membuat object koneksi
        private SqlCommand cmd; // perintah untuk query ke database
        private DataSet ds;
        private SqlDataAdapter da;
        private SqlDataReader dr;

        public PosScreen()
        {
            InitializeComponent();
        }

        private void txtCari_GotFocus(object sender, RoutedEventArgs e)
        {
            txtCari.Text = "";

        }

        private void txtCari_LostFocus(object sender, RoutedEventArgs e)
        {
            txtCari.Text = "Cari makanan, minuman, desserts, dll.";
        }


        private void txtTglPesan_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime dateTime = DateTime.Now;
            string tglPesan = dateTime.ToString("dd MMMM yyyy hh:mm:ss tt", new System.Globalization.CultureInfo("id-ID"));
            txtTglPesan.Content = tglPesan;
        }

        private void dgvMenu_Loaded(object sender, RoutedEventArgs e)
        {
            SqlConnection conn = konn.GetConn();
            conn.Open();
            cmd = new SqlCommand("select nama_menu AS MENU ,harga_menu AS HARGA  from menu", conn); // membuat query ke database
            da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("list menu");
            ds = new DataSet();

            da.Fill(dt);
            dgvMenu.ItemsSource = dt.DefaultView;
            da.Update(dt);
        }

        private void txtCari_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter)
            {
                SqlConnection conn = konn.GetConn();
                conn.Open();
                cmd = new SqlCommand("select nama_menu AS MENU ,harga_menu AS HARGA from menu WHERE nama_menu LIKE '%" + txtCari.Text + "%' ", conn); // membuat query ke database
                da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable("list menu");
                ds = new DataSet();

                da.Fill(dt);
                dgvMenu.ItemsSource = dt.DefaultView;
                da.Update(dt);
            }
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DetailMenu detailMenu = new DetailMenu();
            DataRowView row_selected = (DataRowView)dgvMenu.SelectedItem;
            if (row_selected != null)
            {
                detailMenu.namaMenu.Text = Convert.ToString(row_selected.Row[0]);
                detailMenu.hargaMenu.Text = Convert.ToString(row_selected.Row[1]);
                detailMenu.jmlOrder.Text = "1";
            }

            detailMenu.Show();
            this.Hide();
        }

        private void txtNomerOrder_Loaded(object sender, RoutedEventArgs e)
        {

            SqlConnection conn = konn.GetConn();
            conn.Open();
            cmd = new SqlCommand("SELECT ISNULL(MAX(id_pesanan),0)+1 from pesanan", conn);
            da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            txtNomerOrder.Content = dt.Rows[0][0].ToString();
        }


        private void txtJml1_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        private void btnDashboard_Click(object sender, RoutedEventArgs e)
        {
            DashboardScreen dashboardScreen = new DashboardScreen();
            dashboardScreen.Show();
            this.Close();
        }

        private void btnProduk_Click(object sender, RoutedEventArgs e)
        {
            ProdukScreen produkScreen = new ProdukScreen();
            produkScreen.Show();
            this.Close();
        }

        private void btnPegawai_Click(object sender, RoutedEventArgs e)
        {
            KaryawanScreen karyawan = new KaryawanScreen();
            karyawan.Show();
            this.Close();
        }

        private void btnLaporan_Click(object sender, RoutedEventArgs e)
        {
            LaporanScreen laporanScreen = new LaporanScreen();
            laporanScreen.Show();
            this.Close();
        }
    }
}
