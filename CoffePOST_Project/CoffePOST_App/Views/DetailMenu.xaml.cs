﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoffePOST_App.ViewModels;

using System.Data.SqlClient;

namespace CoffePOST_App.Views
{
    /// <summary>
    /// Interaction logic for DetailMenu.xaml
    /// </summary>
    public partial class DetailMenu : Window
    {
        public DetailMenu()
        {
            InitializeComponent();
        }
        private void btnOrder_Click(object sender, RoutedEventArgs e)
        {
            PosScreen posScreen = new PosScreen();

            posScreen.txtProduk1.Text = namaMenu.Text;
            posScreen.txtJml1.Text = jmlOrder.Text;
            posScreen.hrg.Content = hargaMenu.Text;
            posScreen.Show();
            this.Hide();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            PosScreen posScreen = new PosScreen();

            posScreen.Show();

        }
    }
}
