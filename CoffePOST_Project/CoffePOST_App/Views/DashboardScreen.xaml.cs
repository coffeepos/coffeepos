﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using CoffePOST_App.ViewModels;

using System.Data.SqlClient;
using System.Data;

namespace CoffePOST_App.Views
{
    /// <summary>
    /// Interaction logic for DashboardScreen.xaml
    /// </summary>
    public partial class DashboardScreen : Window
    {
        Koneksi konn = new Koneksi();
        private SqlCommand cmd;
        private DataSet ds;
        private SqlDataAdapter da;
        private SqlDataReader dr;

        public DashboardScreen()
        {
            InitializeComponent();
            ShowDataRiwayat();
            ShowDataProdukTerlaris();
            ShowPendapatan();
            ShowJumlahOrder();
            ShowMenuTerjual();
        }

        void ShowDataRiwayat()
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("SELECT id_pesanan AS 'Nomor Order', total_harga AS 'Total Bayar' FROM pesanan", conn);
            conn.Open();
            da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Info Riwayat");
            ds = new DataSet();

            da.Fill(dt);
            DG_Riwayat.ItemsSource = dt.DefaultView;
            da.Update(dt);
        }

        void ShowDataProdukTerlaris()
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("SELECT nama_menu AS Produk, item_terjual AS Penjualan, (harga_menu*item_terjual) AS Total FROM menu", conn);
            conn.Open();
            da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Info Terlaris");
            ds = new DataSet();

            da.Fill(dt);
            DG_ProdukTerlaris.ItemsSource = dt.DefaultView;
            da.Update(dt);
        }
        void ShowPendapatan()
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("SELECT SUM(total_harga) FROM pesanan WHERE status='selesai'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                TB_Pendapatan.Text = dr[0].ToString();
            }
        }

        void ShowJumlahOrder()
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("SELECT COUNT(id_pesanan) FROM pesanan WHERE status='selesai'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                TB_JumlahOrder.Text = dr[0].ToString();
            }
        }

        void ShowMenuTerjual()
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("SELECT COUNT(DISTINCT id_menu) FROM detail_pesanan", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                TB_MenuTerjual.Text = dr[0].ToString();
            }
        }
        private void DG_ProdukTerlaris_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DG_Riwayat_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TB_Pendapatan_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void btnPOS_Selected(object sender, RoutedEventArgs e)
        {
            PosScreen logscreen = new PosScreen();
            logscreen.Show();
            this.Close();
        }

        private void btn_Produk_Selected(object sender, RoutedEventArgs e)
        {
            ProdukScreen proscreen = new ProdukScreen();
            proscreen.Show();
            this.Close();
        }

        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {
            KaryawanScreen karSec = new KaryawanScreen();
            karSec.Show();
            this.Close();
        }

        private void ListViewItem_Selected_1(object sender, RoutedEventArgs e)
        {
            LaporanScreen lapsec = new LaporanScreen();
            lapsec.Show();
            this.Close();
        }
    }
}
