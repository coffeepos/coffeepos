﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoffePOST_App.ViewModels;


using System.Data;
using System.Data.SqlClient;

namespace CoffePOST_App.Views
{
    /// <summary>
    /// Interaction logic for ProdukScreen.xaml
    /// </summary>
    public partial class ProdukScreen : Window
    {
        Koneksi konn = new Koneksi();
        private SqlCommand cmd;
        private DataSet ds;
        private SqlDataAdapter da;
        private SqlDataReader dr;

        void KondisiAwal()
        {
            Input_Nama.Text = "";
            Input_IDroduk.Text = "";
            Input_Harga.Text = "";
            Input_Stok.Text = "";
            TampilkanDataProduk();
        }
        public ProdukScreen()
        {
            InitializeComponent();
            KondisiAwal();
        }
        void TampilkanDataProduk()
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("SELECT id_menu AS 'ID MENU', nama_menu AS 'NAMA PRODUK', stok AS 'STOK', harga_menu AS 'HARGA', id_jenis AS 'JENIS PRODUK' FROM menu", conn);
            conn.Open();
            da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable("Info Data Produk");
            ds = new DataSet();

            da.Fill(dt);
            DG_TampilData.ItemsSource = dt.DefaultView;
            da.Update(dt);
        }
        private void Btn_Insert_Click(object sender, RoutedEventArgs e)
        {
            if (Input_IDroduk.Text == "" || Input_Nama.Text == "" || Input_Harga.Text == "" || Input_Stok.Text == "" || ComboJenis.Text == "")
            {
                MessageBox.Show("Mohon Lengkapi form!");
            }
            else if (Input_IDroduk.Text.Length < 5)
            {
                MessageBox.Show("ID Produk minimal 5 character");
            }
            else
            {
                SqlConnection conn = konn.GetConn();
                cmd = new SqlCommand("insert into menu(id_menu, nama_menu, stok, harga_menu, id_jenis) values('" + Input_IDroduk.Text + "','" + Input_Nama.Text + "'," + Input_Stok.Text + "," + Input_Harga.Text + ",'" + ComboJenis.Text + "')", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data berhasil ditambahkan!");
                KondisiAwal();
            }
        }

        private void DG_TampilData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ComboJenis_Loaded(object sender, RoutedEventArgs e)
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("SELECT * FROM jenis_menu", conn);
            conn.Open();
            //cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                string id_jenis = dr.GetString(0);
                ComboJenis.Items.Add(id_jenis);
            }
            conn.Close();
        }

        private void Btn_Update_Click(object sender, RoutedEventArgs e)
        {
            if (Input_IDroduk.Text == "" || Input_Nama.Text == "" || Input_Harga.Text == "" || Input_Stok.Text == "" || ComboJenis.Text == "")
            {
                MessageBox.Show("Mohon Lengkapi form!");
            }
            else if (Input_IDroduk.Text.Length < 5)
            {
                MessageBox.Show("ID Produk minimal 5 character");
            }
            else
            {
                SqlConnection conn = konn.GetConn();
                cmd = new SqlCommand("UPDATE menu SET nama_menu='" + Input_Nama.Text + "', stok=" + Input_Stok.Text + ", harga_menu=" + Input_Harga.Text + ", id_jenis='" + ComboJenis.Text + "' WHERE id_menu='" + Input_IDroduk.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data berhasil diubah!");
                KondisiAwal();
            }
        }

        private void Input_IDroduk_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SqlConnection conn = konn.GetConn();
                cmd = new SqlCommand("SELECT id_menu, nama_menu, stok, harga_menu, id_jenis FROM menu WHERE id_menu='" + Input_IDroduk.Text + "'", conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    Input_IDroduk.Text = dr[0].ToString();
                    Input_Nama.Text = dr[1].ToString();
                    Input_Stok.Text = dr[2].ToString();
                    Input_Harga.Text = dr[3].ToString();
                    ComboJenis.Text = dr[4].ToString();
                }
            }
        }
        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataRowView row_selected = (DataRowView)DG_TampilData.SelectedItem;
            if (row_selected != null)
            {
                Input_IDroduk.Text = Convert.ToString(row_selected.Row[0]);
                Input_Nama.Text = Convert.ToString(row_selected.Row[1]);
                Input_Stok.Text = Convert.ToString(row_selected.Row[2]);
                Input_Harga.Text = Convert.ToString(row_selected.Row[3]);
                ComboJenis.Text = Convert.ToString(row_selected.Row[4]);
            }
        }

        private void Btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection conn = konn.GetConn();
            cmd = new SqlCommand("DELETE FROM menu WHERE id_menu='" + Input_IDroduk.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data berhasil dihapus!");
            KondisiAwal();
        }

        private void Input_Nama_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btn_dashboard_Selected(object sender, RoutedEventArgs e)
        {
            DashboardScreen dasboard = new DashboardScreen();
            dasboard.Show();
            this.Close();
        }
    }
}
